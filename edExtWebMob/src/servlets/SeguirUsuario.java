package servlets;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.rpc.ServiceException;

import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;
import publicadores.DtUsuario;

/**
 * Servlet implementation class AltaCurso
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024, maxFileSize = 1024 * 1024 * 5, maxRequestSize = 1024 * 1024 * 5 * 5)

@WebServlet("/SeguirUsuario")
public class SeguirUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SeguirUsuario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher rd;
		doGet(request, response);
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		ControladorPublish port;
		try {
			port = cps.getControladorPublishPort();
			
			String nickNameSeguidor = request.getParameter("nickNameSeguidor");
			String nickNameSeguido = request.getParameter("nickNameSeguido");
			DtUsuario usuSeguido = port.seleccionarDtUsuario(nickNameSeguido);
			
			HttpSession miSesion = request.getSession(true);
			List<String> usuario = (List<String>)miSesion.getAttribute("usuariosSeguidos");
			
			
			boolean esta = false;
			if(usuario != null) {
				for(String usu: usuario) {
					if(nickNameSeguido.equals(usu)) {
						esta = true;
					}
				}
			}
			
			
			if(esta) {
				List<String> usuario1 = new ArrayList<String>();
				List<String> usuario2 = (List<String>)miSesion.getAttribute("usuariosSeguidos");
				//iconusu.dejarSeguirUsuario(nickNameSeguidor, nickNameSeguido);
				port.dejarSeguirUsuario(nickNameSeguidor, nickNameSeguido);
				System.out.println("Dejo de seguir a "+ nickNameSeguido);
				if(usuario2 != null) {
					for(String u: usuario2) {
						if(!u.equals(usuSeguido.getNickName())) {
							usuario1.add(u);
						}
					}
				}
				
		        miSesion.removeAttribute("usuariosSeguidos");
		        miSesion.setAttribute("usuariosSeguidos", usuario1);
		        
		        for(String us: usuario1) {
		        	System.out.println("Esto deberia remover un usuario seguidos " + us);
		        }
			}
			else {
	        	

				List<String> usuario1 = new ArrayList<String>();
				List<String> usuario2 = (List<String>)miSesion.getAttribute("usuariosSeguidos");
				//iconusu.seguirUsuario(nickNameSeguidor, nickNameSeguido);
				port.seguirUsuario(nickNameSeguidor, nickNameSeguido);
				if(usuario2 != null) {
					for(String u: usuario2) {
						usuario1.add(u);
					}
				}
				
				usuario1.add(usuSeguido.getNickName());
		        miSesion.setAttribute("usuariosSeguidos", usuario1);
		        System.out.println("Entre ACAAAAAAAAAAAAA!!!!!!!!!!!!!!!!!" + usuario1.toString());
		        for(String us: usuario1) {
		        	System.out.println("Esto deberia agregar un usuario seguido" + us);
		        }
			}
			
			rd = request.getRequestDispatcher("consultaUsuario.jsp");
	        rd.forward(request,response);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
			
		
	public static boolean seguirUsuario(String nickNameSeguidor, String nickNameSeguido) throws RemoteException, ServiceException {
		System.out.println("nickName Seguidor = " + nickNameSeguidor);
		System.out.println("nickName a Seguir = " + nickNameSeguido);
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		ControladorPublish port = cps.getControladorPublishPort();
		
		if(port.checkSeguidor(nickNameSeguidor, nickNameSeguido)) {
			return true;
		}
		return false;
	}
	

}


