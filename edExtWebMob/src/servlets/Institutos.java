package servlets;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import publicadores.DtCurso;
import publicadores.DtEdicion;
import publicadores.DtPrograma;
import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;

/**
 * Servlet implementation class Prueba
 */
@WebServlet("/Institutos")
public class Institutos extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Institutos() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		String[] categorias= {"falsa","falsa"};
		String[] listInstitutos={"falso"};
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		try {
			ControladorPublish	port = cps.getControladorPublishPort();
			categorias = port.listarCategorias();
			 listInstitutos =  port.listarInstitutosArray();
			
			String insSel= (String) request.getParameter("inselecc");
			String catSel= (String) request.getParameter("catselecc");
			System.out.println("Servlet.Cursos.doGet " +insSel );
			List<String> listCategorias = Arrays.asList(categorias); //iconip.listarCategorias();
			request.setAttribute("listCategorias", listCategorias);
			
			request.setAttribute("listInstitutos", listInstitutos);
			request.setAttribute("inicio","vuelve");
			if(insSel!=null) {
				String[] listaCursos = port.listarNombreCursos(insSel);//iconip.listarNombreCursos(insSel);
				DtCurso[] listdtCursos = new DtCurso[listaCursos.length];
				int topesEdi[]= new int[listaCursos.length];
				int topesProg[]= new int[listaCursos.length];
				DtEdicion[][] listBidtEdiciones= new DtEdicion[listaCursos.length][12];
				DtPrograma[][] listBidtProgramas = new DtPrograma[listaCursos.length][12];
				if(listaCursos.length!=0 && listaCursos!=null) {
									
					for(int i=0; i < listaCursos.length;  i++) {
						listdtCursos[i]= port.mostrarDatosdeCurso(listaCursos[i]);//iconip.mostrarDatosdeCurso(listaCursos[i]);
						String[] listaediciones=port.listarEdicionesdeCurso(listaCursos[i]);
						topesEdi[i]=listaediciones.length;
						for(int j=0; j < listaediciones.length;  j++) {
							System.out.println("-" + listaediciones[j] + "-");
							listBidtEdiciones[i][j]=port.mostrarEdicion(listaediciones[j]);
							System.out.println("DTE Instituto servlet: " + listBidtEdiciones[i][j].getFechaI() );
						}
						int h=0;
						DtPrograma[] programas=port.programasdeCurso(listaCursos[i]);
						for(int j=0; j < programas.length;  j++) {
							listBidtProgramas[i][j]=programas[j];
							h++;
						}
						topesProg[i]=h;
						//System.out.println("Servlet.Cursos.doGet listdtCursos[i] " +listdtCursos[i].getFoto() );
	
					}
						
					request.setAttribute("topesProg", topesProg);				
					request.setAttribute("listBidtProgramas", listBidtProgramas);
					request.setAttribute("listdtCursos", listdtCursos);
					request.setAttribute("listBidtEdiciones",listBidtEdiciones);
					request.setAttribute("topesEdi", topesEdi);
				}else {
					
				}
				request.setAttribute("nombreInstituto", insSel);
				request.setAttribute("nomCurss", listaCursos);
			}else if(catSel!=null){
				//System.out.print("CATSELECC "+ catSel);
				DtCurso[] listdtCursos = port.cursosdeCategoria(catSel);
				DtPrograma[][] listBidtProgramas = new DtPrograma[listdtCursos.length][12];
				//for(DtCurso dtc:listdtCursos) {System.out.print("dtc "+ dtc.getNombre());}
				request.setAttribute("listdtCursos", listdtCursos);
				int topesEdi[]= new int[listdtCursos.length];for(int i:topesEdi) topesEdi[i]=0;
				int topesProg[]= new int[listdtCursos.length];for(int i:topesProg) topesProg[i]=0;
				DtEdicion[][] listBidtEdiciones= new DtEdicion[listdtCursos.length][12];
				for(int i=0; i < listdtCursos.length;  i++) {
					DtEdicion[] dtediciones=port.mostrarDtEdicionesArray(listdtCursos[i].getNombre());
					int h=0;
					for(int j=0; j < dtediciones.length;  j++) {
						listBidtEdiciones[i][j]=dtediciones[j];
						h++;
					}
					topesEdi[i]=h;
					DtPrograma[] dtprogramas= port.programasdeCurso(listdtCursos[i].getNombre());
					h=0;
					for(int j=0; j < dtprogramas.length;  j++) {
						listBidtProgramas[i][j]=dtprogramas[j];
						h++;
					}
					topesProg[i]=h;
					
				}
				request.setAttribute("listBidtProgramas", listBidtProgramas);
				request.setAttribute("listBidtEdiciones",listBidtEdiciones);
				request.setAttribute("topesProg", topesProg);
				request.setAttribute("topesEdi", topesEdi);
				request.setAttribute("listdtCursos", listdtCursos);
			}else {
				//aca hay que manejar el caso de ver un curso si viene desde la categoria
			}
			//for(String s:listInstitutos) System.out.println(s);
	
			RequestDispatcher rd;
			rd = request.getRequestDispatcher("institutos.jsp");
			rd.forward(request, response);
		}catch(ServiceException e) {
			String text = "Error en  getControladorPublishPort Institutos.java";
			response.setContentType("text/plain");
			response.getWriter().write(text);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
