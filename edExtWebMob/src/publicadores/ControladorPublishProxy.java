package publicadores;

public class ControladorPublishProxy implements publicadores.ControladorPublish {
  private String _endpoint = null;
  private publicadores.ControladorPublish controladorPublish = null;
  
  public ControladorPublishProxy() {
    _initControladorPublishProxy();
  }
  
  public ControladorPublishProxy(String endpoint) {
    _endpoint = endpoint;
    _initControladorPublishProxy();
  }
  
  private void _initControladorPublishProxy() {
    try {
      controladorPublish = (new publicadores.ControladorPublishServiceLocator()).getControladorPublishPort();
      if (controladorPublish != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)controladorPublish)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)controladorPublish)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (controladorPublish != null)
      ((javax.xml.rpc.Stub)controladorPublish)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public publicadores.ControladorPublish getControladorPublish() {
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish;
  }
  
  public void seleccionarCursoInstancia(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.seleccionarCursoInstancia(arg0);
  }
  
  public publicadores.DtDocente seleccionarDtDocente(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.seleccionarDtDocente(arg0);
  }
  
  public void altaUsuarioEstudiante(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.util.Calendar arg4, boolean arg5, java.lang.String arg6, java.lang.String arg7) throws java.rmi.RemoteException, publicadores.EmailExistenteException, publicadores.NickNameExistenteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.altaUsuarioEstudiante(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
  }
  
  public publicadores.DtEdicion[] mostrarDtEdicionesArray(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.mostrarDtEdicionesArray(arg0);
  }
  
  public java.lang.String rutaAnombreDeImagen(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.rutaAnombreDeImagen(arg0);
  }
  
  public java.lang.String[] listarInstitutosArray() throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.listarInstitutosArray();
  }
  
  public java.lang.String[] listarNombreCursos(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.listarNombreCursos(arg0);
  }
  
  public java.lang.String[] listarUsuarios() throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.listarUsuarios();
  }
  
  public publicadores.Usuario seleccionarUsuario(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.seleccionarUsuario(arg0);
  }
  
  public java.lang.String[] listarProgramas() throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.listarProgramas();
  }
  
  public boolean modificarDatos(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.util.Calendar arg4) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.modificarDatos(arg0, arg1, arg2, arg3, arg4);
  }
  
  public java.lang.String[] cursosDePrograma() throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.cursosDePrograma();
  }
  
  public publicadores.DtCurso seleccionarCurso(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.seleccionarCurso(arg0);
  }
  
  public boolean loginNickname(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException, publicadores.UsuarioNoExisteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.loginNickname(arg0, arg1);
  }
  
  public boolean loginEmail(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.loginEmail(arg0, arg1);
  }
  
  public void altaUsuarioDocente(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.util.Calendar arg4, boolean arg5, java.lang.String arg6, java.lang.String arg7, java.lang.String arg8) throws java.rmi.RemoteException, publicadores.EmailExistenteException, publicadores.NickNameExistenteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.altaUsuarioDocente(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
  }
  
  public publicadores.DtUsuario seleccionarDtUsuario(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.seleccionarDtUsuario(arg0);
  }
  
  public java.lang.String[] seleccionarInscripcionED(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.seleccionarInscripcionED(arg0);
  }
  
  public java.lang.String[] listarEdicionesdeCurso(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.listarEdicionesdeCurso(arg0);
  }
  
  public boolean nuevoCurso(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.nuevoCurso(arg0);
  }
  
  public void setearCurso(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, int arg3, int arg4, java.lang.String arg5, java.util.Calendar arg6, java.lang.String arg7) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.setearCurso(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
  }
  
  public void seleccionarInstituto(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.seleccionarInstituto(arg0);
  }
  
  public publicadores.ArrayList listarCategoriasList() throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.listarCategoriasList();
  }
  
  public publicadores.DtInscripcionEdc[] obtenerInscripciones(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, boolean arg3) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.obtenerInscripciones(arg0, arg1, arg2, arg3);
  }
  
  public publicadores.DtInscripcionEdc[] inscrpcionesAceptadas(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.inscrpcionesAceptadas(arg0, arg1, arg2);
  }
  
  public void setearInscripciones(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, publicadores.DtInscripcionEdc[] arg3) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.setearInscripciones(arg0, arg1, arg2, arg3);
  }
  
  public publicadores.DtCurso mostrarDatosdeCurso(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.mostrarDatosdeCurso(arg0);
  }
  
  public java.lang.String[] listarCategorias() throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.listarCategorias();
  }
  
  public publicadores.DtPrograma[] programasdeCurso(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.programasdeCurso(arg0);
  }
  
  public void pruebaDate(java.util.Calendar arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.pruebaDate(arg0);
  }
  
  public java.lang.String institutoDeCurso(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.institutoDeCurso(arg0);
  }
  
  public void seleccionarPrevias(java.lang.String[] arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.seleccionarPrevias(arg0);
  }
  
  public publicadores.DtEdicion mostrarEdicion(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.mostrarEdicion(arg0);
  }
  
  public publicadores.DtCurso[] cursosdeCategoria(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.cursosdeCategoria(arg0);
  }
  
  public boolean altaEdicionCurso(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.util.Calendar arg3, java.util.Calendar arg4, int arg5, java.util.Calendar arg6, publicadores.Docente[] arg7, java.lang.String arg8) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.altaEdicionCurso(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
  }
  
  public void altaInscripcionEdc(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException, publicadores.InscripcionEdicionRepetida{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.altaInscripcionEdc(arg0, arg1);
  }
  
  public java.lang.String emailToNick(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.emailToNick(arg0);
  }
  
  public java.lang.String[] listarDocentes() throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.listarDocentes();
  }
  
  public void altaPrograma(java.lang.String arg0, java.lang.String arg1, java.util.Calendar arg2, java.util.Calendar arg3, java.lang.String arg4) throws java.rmi.RemoteException, publicadores.IOException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.altaPrograma(arg0, arg1, arg2, arg3, arg4);
  }
  
  public boolean checkSeguidor(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.checkSeguidor(arg0, arg1);
  }
  
  public void dejarSeguirUsuario(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException, publicadores.Exception{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.dejarSeguirUsuario(arg0, arg1);
  }
  
  public java.lang.String[] listarCategoriasPF(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.listarCategoriasPF(arg0);
  }
  
  public publicadores.DtPrograma verPrograma(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.verPrograma(arg0);
  }
  
  public void seguirUsuario(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException, publicadores.Exception{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.seguirUsuario(arg0, arg1);
  }
  
  public java.lang.String[] usuariosSeguidos(java.lang.String arg0) throws java.rmi.RemoteException, publicadores.IOException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.usuariosSeguidos(arg0);
  }
  
  public publicadores.DtEdicion traerUltimaEdicion(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException, publicadores.SinEdicionException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    return controladorPublish.traerUltimaEdicion(arg0, arg1);
  }
  
  public void confirmarAltaCurso() throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.confirmarAltaCurso();
  }
  
  public void setearCategorias(java.lang.String[] arg0) throws java.rmi.RemoteException{
    if (controladorPublish == null)
      _initControladorPublishProxy();
    controladorPublish.setearCategorias(arg0);
  }
  
  
}