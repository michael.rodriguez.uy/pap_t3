/**
 * Usuario.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package publicadores;

public abstract class Usuario  implements java.io.Serializable {
    private java.lang.String apellido;

    private java.lang.String correo;

    private java.lang.Boolean esDocente;

    private java.util.Calendar fechaNacimiento;

    private java.lang.String imagen;

    private byte[] img;

    private publicadores.InscripcionEdc[] inscripciones;

    private java.lang.String nickName;

    private java.lang.String nombre;

    private java.lang.String password;

    private publicadores.Usuario[] usuariosSeguidos;

    public Usuario() {
    }

    public Usuario(
           java.lang.String apellido,
           java.lang.String correo,
           java.lang.Boolean esDocente,
           java.util.Calendar fechaNacimiento,
           java.lang.String imagen,
           byte[] img,
           publicadores.InscripcionEdc[] inscripciones,
           java.lang.String nickName,
           java.lang.String nombre,
           java.lang.String password,
           publicadores.Usuario[] usuariosSeguidos) {
           this.apellido = apellido;
           this.correo = correo;
           this.esDocente = esDocente;
           this.fechaNacimiento = fechaNacimiento;
           this.imagen = imagen;
           this.img = img;
           this.inscripciones = inscripciones;
           this.nickName = nickName;
           this.nombre = nombre;
           this.password = password;
           this.usuariosSeguidos = usuariosSeguidos;
    }


    /**
     * Gets the apellido value for this Usuario.
     * 
     * @return apellido
     */
    public java.lang.String getApellido() {
        return apellido;
    }


    /**
     * Sets the apellido value for this Usuario.
     * 
     * @param apellido
     */
    public void setApellido(java.lang.String apellido) {
        this.apellido = apellido;
    }


    /**
     * Gets the correo value for this Usuario.
     * 
     * @return correo
     */
    public java.lang.String getCorreo() {
        return correo;
    }


    /**
     * Sets the correo value for this Usuario.
     * 
     * @param correo
     */
    public void setCorreo(java.lang.String correo) {
        this.correo = correo;
    }


    /**
     * Gets the esDocente value for this Usuario.
     * 
     * @return esDocente
     */
    public java.lang.Boolean getEsDocente() {
        return esDocente;
    }


    /**
     * Sets the esDocente value for this Usuario.
     * 
     * @param esDocente
     */
    public void setEsDocente(java.lang.Boolean esDocente) {
        this.esDocente = esDocente;
    }


    /**
     * Gets the fechaNacimiento value for this Usuario.
     * 
     * @return fechaNacimiento
     */
    public java.util.Calendar getFechaNacimiento() {
        return fechaNacimiento;
    }


    /**
     * Sets the fechaNacimiento value for this Usuario.
     * 
     * @param fechaNacimiento
     */
    public void setFechaNacimiento(java.util.Calendar fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }


    /**
     * Gets the imagen value for this Usuario.
     * 
     * @return imagen
     */
    public java.lang.String getImagen() {
        return imagen;
    }


    /**
     * Sets the imagen value for this Usuario.
     * 
     * @param imagen
     */
    public void setImagen(java.lang.String imagen) {
        this.imagen = imagen;
    }


    /**
     * Gets the img value for this Usuario.
     * 
     * @return img
     */
    public byte[] getImg() {
        return img;
    }


    /**
     * Sets the img value for this Usuario.
     * 
     * @param img
     */
    public void setImg(byte[] img) {
        this.img = img;
    }


    /**
     * Gets the inscripciones value for this Usuario.
     * 
     * @return inscripciones
     */
    public publicadores.InscripcionEdc[] getInscripciones() {
        return inscripciones;
    }


    /**
     * Sets the inscripciones value for this Usuario.
     * 
     * @param inscripciones
     */
    public void setInscripciones(publicadores.InscripcionEdc[] inscripciones) {
        this.inscripciones = inscripciones;
    }

    public publicadores.InscripcionEdc getInscripciones(int i) {
        return this.inscripciones[i];
    }

    public void setInscripciones(int i, publicadores.InscripcionEdc _value) {
        this.inscripciones[i] = _value;
    }


    /**
     * Gets the nickName value for this Usuario.
     * 
     * @return nickName
     */
    public java.lang.String getNickName() {
        return nickName;
    }


    /**
     * Sets the nickName value for this Usuario.
     * 
     * @param nickName
     */
    public void setNickName(java.lang.String nickName) {
        this.nickName = nickName;
    }


    /**
     * Gets the nombre value for this Usuario.
     * 
     * @return nombre
     */
    public java.lang.String getNombre() {
        return nombre;
    }


    /**
     * Sets the nombre value for this Usuario.
     * 
     * @param nombre
     */
    public void setNombre(java.lang.String nombre) {
        this.nombre = nombre;
    }


    /**
     * Gets the password value for this Usuario.
     * 
     * @return password
     */
    public java.lang.String getPassword() {
        return password;
    }


    /**
     * Sets the password value for this Usuario.
     * 
     * @param password
     */
    public void setPassword(java.lang.String password) {
        this.password = password;
    }


    /**
     * Gets the usuariosSeguidos value for this Usuario.
     * 
     * @return usuariosSeguidos
     */
    public publicadores.Usuario[] getUsuariosSeguidos() {
        return usuariosSeguidos;
    }


    /**
     * Sets the usuariosSeguidos value for this Usuario.
     * 
     * @param usuariosSeguidos
     */
    public void setUsuariosSeguidos(publicadores.Usuario[] usuariosSeguidos) {
        this.usuariosSeguidos = usuariosSeguidos;
    }

    public publicadores.Usuario getUsuariosSeguidos(int i) {
        return this.usuariosSeguidos[i];
    }

    public void setUsuariosSeguidos(int i, publicadores.Usuario _value) {
        this.usuariosSeguidos[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Usuario)) return false;
        Usuario other = (Usuario) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.apellido==null && other.getApellido()==null) || 
             (this.apellido!=null &&
              this.apellido.equals(other.getApellido()))) &&
            ((this.correo==null && other.getCorreo()==null) || 
             (this.correo!=null &&
              this.correo.equals(other.getCorreo()))) &&
            ((this.esDocente==null && other.getEsDocente()==null) || 
             (this.esDocente!=null &&
              this.esDocente.equals(other.getEsDocente()))) &&
            ((this.fechaNacimiento==null && other.getFechaNacimiento()==null) || 
             (this.fechaNacimiento!=null &&
              this.fechaNacimiento.equals(other.getFechaNacimiento()))) &&
            ((this.imagen==null && other.getImagen()==null) || 
             (this.imagen!=null &&
              this.imagen.equals(other.getImagen()))) &&
            ((this.img==null && other.getImg()==null) || 
             (this.img!=null &&
              java.util.Arrays.equals(this.img, other.getImg()))) &&
            ((this.inscripciones==null && other.getInscripciones()==null) || 
             (this.inscripciones!=null &&
              java.util.Arrays.equals(this.inscripciones, other.getInscripciones()))) &&
            ((this.nickName==null && other.getNickName()==null) || 
             (this.nickName!=null &&
              this.nickName.equals(other.getNickName()))) &&
            ((this.nombre==null && other.getNombre()==null) || 
             (this.nombre!=null &&
              this.nombre.equals(other.getNombre()))) &&
            ((this.password==null && other.getPassword()==null) || 
             (this.password!=null &&
              this.password.equals(other.getPassword()))) &&
            ((this.usuariosSeguidos==null && other.getUsuariosSeguidos()==null) || 
             (this.usuariosSeguidos!=null &&
              java.util.Arrays.equals(this.usuariosSeguidos, other.getUsuariosSeguidos())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getApellido() != null) {
            _hashCode += getApellido().hashCode();
        }
        if (getCorreo() != null) {
            _hashCode += getCorreo().hashCode();
        }
        if (getEsDocente() != null) {
            _hashCode += getEsDocente().hashCode();
        }
        if (getFechaNacimiento() != null) {
            _hashCode += getFechaNacimiento().hashCode();
        }
        if (getImagen() != null) {
            _hashCode += getImagen().hashCode();
        }
        if (getImg() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getImg());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getImg(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getInscripciones() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getInscripciones());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getInscripciones(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getNickName() != null) {
            _hashCode += getNickName().hashCode();
        }
        if (getNombre() != null) {
            _hashCode += getNombre().hashCode();
        }
        if (getPassword() != null) {
            _hashCode += getPassword().hashCode();
        }
        if (getUsuariosSeguidos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getUsuariosSeguidos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getUsuariosSeguidos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Usuario.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://publicadores/", "usuario"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("apellido");
        elemField.setXmlName(new javax.xml.namespace.QName("", "apellido"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("correo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "correo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("esDocente");
        elemField.setXmlName(new javax.xml.namespace.QName("", "esDocente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaNacimiento");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fechaNacimiento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("imagen");
        elemField.setXmlName(new javax.xml.namespace.QName("", "imagen"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("img");
        elemField.setXmlName(new javax.xml.namespace.QName("", "img"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inscripciones");
        elemField.setXmlName(new javax.xml.namespace.QName("", "inscripciones"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://publicadores/", "inscripcionEdc"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nickName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nickName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nombre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nombre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("password");
        elemField.setXmlName(new javax.xml.namespace.QName("", "password"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuariosSeguidos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuariosSeguidos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://publicadores/", "usuario"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
