/**
 * DtInscripcionEdc.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package publicadores;

public class DtInscripcionEdc  implements java.io.Serializable {
    private publicadores.DtEdicion edicion;

    private publicadores.DtUsuario estudiante;

    private java.util.Calendar fecha;

    private publicadores.EstadoInscripcionEdc estado;

    public DtInscripcionEdc() {
    }

    public DtInscripcionEdc(
           publicadores.DtEdicion edicion,
           publicadores.DtUsuario estudiante,
           java.util.Calendar fecha,
           publicadores.EstadoInscripcionEdc estado) {
           this.edicion = edicion;
           this.estudiante = estudiante;
           this.fecha = fecha;
           this.estado = estado;
    }


    /**
     * Gets the edicion value for this DtInscripcionEdc.
     * 
     * @return edicion
     */
    public publicadores.DtEdicion getEdicion() {
        return edicion;
    }


    /**
     * Sets the edicion value for this DtInscripcionEdc.
     * 
     * @param edicion
     */
    public void setEdicion(publicadores.DtEdicion edicion) {
        this.edicion = edicion;
    }


    /**
     * Gets the estudiante value for this DtInscripcionEdc.
     * 
     * @return estudiante
     */
    public publicadores.DtUsuario getEstudiante() {
        return estudiante;
    }


    /**
     * Sets the estudiante value for this DtInscripcionEdc.
     * 
     * @param estudiante
     */
    public void setEstudiante(publicadores.DtUsuario estudiante) {
        this.estudiante = estudiante;
    }


    /**
     * Gets the fecha value for this DtInscripcionEdc.
     * 
     * @return fecha
     */
    public java.util.Calendar getFecha() {
        return fecha;
    }


    /**
     * Sets the fecha value for this DtInscripcionEdc.
     * 
     * @param fecha
     */
    public void setFecha(java.util.Calendar fecha) {
        this.fecha = fecha;
    }


    /**
     * Gets the estado value for this DtInscripcionEdc.
     * 
     * @return estado
     */
    public publicadores.EstadoInscripcionEdc getEstado() {
        return estado;
    }


    /**
     * Sets the estado value for this DtInscripcionEdc.
     * 
     * @param estado
     */
    public void setEstado(publicadores.EstadoInscripcionEdc estado) {
        this.estado = estado;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DtInscripcionEdc)) return false;
        DtInscripcionEdc other = (DtInscripcionEdc) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.edicion==null && other.getEdicion()==null) || 
             (this.edicion!=null &&
              this.edicion.equals(other.getEdicion()))) &&
            ((this.estudiante==null && other.getEstudiante()==null) || 
             (this.estudiante!=null &&
              this.estudiante.equals(other.getEstudiante()))) &&
            ((this.fecha==null && other.getFecha()==null) || 
             (this.fecha!=null &&
              this.fecha.equals(other.getFecha()))) &&
            ((this.estado==null && other.getEstado()==null) || 
             (this.estado!=null &&
              this.estado.equals(other.getEstado())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEdicion() != null) {
            _hashCode += getEdicion().hashCode();
        }
        if (getEstudiante() != null) {
            _hashCode += getEstudiante().hashCode();
        }
        if (getFecha() != null) {
            _hashCode += getFecha().hashCode();
        }
        if (getEstado() != null) {
            _hashCode += getEstado().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DtInscripcionEdc.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://publicadores/", "dtInscripcionEdc"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("edicion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "edicion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://publicadores/", "dtEdicion"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estudiante");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estudiante"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://publicadores/", "dtUsuario"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fecha");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fecha"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estado");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://publicadores/", "estadoInscripcionEdc"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
