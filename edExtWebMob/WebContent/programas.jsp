<%@page import="java.util.Calendar"%>
<%@page import="publicadores.DtCurso"%>
<%@page import="publicadores.ControladorPublish"%>
<%@page import="publicadores.ControladorPublishServiceLocator"%>
<%@page import="publicadores.ControladorPublishService"%>
<%@page import="publicadores.DtPrograma"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="header.jsp"%>
<meta charset="ISO-8859-1">
<title>Info Programas</title>
</head>
<body>


	<%
		HttpSession sesion = request.getSession();
		if (sesion.getAttribute("conectado") == null) {
	%><%@include file="navbarSinSession.jsp"%>
	<%
		} else {
	%><%@include file="navbarSession.jsp"%>
	<%
		}

		//List<String> listaCategoria = request.getAttribute("listCategorias");
	%>
	<%
		if (request.getAttribute("inicio") == null && request.getAttribute("inicio") != "vuelve") {
			RequestDispatcher rd;
			request.setAttribute("inicio", "si");
			rd = request.getRequestDispatcher("Programas");
			rd.forward(request, response);
		}
	%>
	
	
	<div class="container-fluid mt-5">
		<div class="row">
			<div class="col-3">
				<%@include file="menuIzq.jsp"%>
			</div>
			<%
			DtPrograma programa = (DtPrograma)request.getAttribute("programa");
			
			String[] cursos = (String[])request.getAttribute("cursos");
			
			String[] categorias = (String[])request.getAttribute("categorias");

			%>

			<div class="col-9">
				<div id="mensajes"></div>
				<div class="card">
					<div class="card-body titulo">
					<% 
						out.println("<h2 class='d-inline'>" + programa.getNombre() + "</h2>");
					%>
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-lg-4">						
							<ul class="list-group list-group-horizontal">
								<ul class="list-group">
								  <li class="list-group-item">Nombre</li>
								  <li class="list-group-item">Descripcion</li>
								  <li class="list-group-item">Fecha inicial</li>
								  <li class="list-group-item">Fecha final</li>
								</ul>
								<ul class="list-group">
									<%
									Calendar fechaI = programa.getFechaI();	
									
									int diaI = fechaI.getTime().getDate();
									int mesI = fechaI.getTime().getMonth() + 1;
									int anioI = fechaI.getTime().getYear() + 1900;
									
									Calendar fechaF = programa.getFechaF();
									
									int diaF = fechaF.getTime().getDate();
									int mesF = fechaF.getTime().getMonth() + 1;
									int anioF = fechaF.getTime().getYear() + 1900;
									
									%>
								  <li class="list-group-item"><%=programa.getNombre()%></li>
								  <li class="list-group-item"><%=programa.getDescripcion()%></li>
								  <li class="list-group-item"><%out.println(diaI + "/" + mesI + "/" + anioI);%></li>
								  <li class="list-group-item"><%out.println(diaF + "/" + mesF + "/" + anioF);%></li>
								</ul>
							</ul>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-body">
					Cursos
						<div class="row">
							<%
								//String[] listaCursos = (String[]) request.getAttribute("nomCurss");
								if (cursos.length > 0) {
									for (String c: cursos) {
										ControladorPublishService cps = new ControladorPublishServiceLocator();
										ControladorPublish port;
										port = cps.getControladorPublishPort();
										DtCurso curSelecc = port.seleccionarCurso(c);
										String nombreInstituto = port.institutoDeCurso(c);
										sesion.setAttribute("inselecc", nombreInstituto);
										sesion.setAttribute("curselecc", c);
										sesion.setAttribute("inselecc", nombreInstituto);
										sesion.setAttribute("catSel", categorias);
							%>
							<div class="col-md-6 col-sm-6 col-lg-4">
								<div class="card">
									<%
										if (curSelecc.getFoto() != null) {
													out.println("<img src='" + "imagenes/cursos/" + curSelecc.getFoto()
															+ "' class='card-img-top img-fluid' alt='Imagen not found' >");
												} else {
													out.println(
															"<img src='imagenes/cursos/cursodefault.jpg' class='card-img-top' alt='Imagen not found'>");
												}
									%>
									<div class="card-body">
										<%
											out.println("<h5 class='card-title'><a  href='Cursos?inselecc=" + nombreInstituto + "&curselecc="
															+ curSelecc.getNombre() + "'>" + curSelecc.getNombre() + "</a></h5>");
													out.println("<p class='card-text'>" + curSelecc.getDescripcion() + "</p>");

													out.println("<a class='btn btn-block btn-sm btn-outline-secondary' href='Cursos?inselecc="
															+ nombreInstituto + "&curselecc=" + curSelecc.getNombre() + "'> Ir a Curso</a>");
										%>
									</div>
								</div>
							</div>
							<%
								} // for
								} else {
									out.println(
											"<div class='col-sm-12'><div class='alert alert-danger' role='alert'>No hay Cursos asociados al programa.</div>");
								}
							%>
						</div>
							
					</div>
					Categorias
									<ul class="list-group list-group-horizontal">
									<%
									ControladorPublishService cps = new ControladorPublishServiceLocator();
									ControladorPublish port;
									port = cps.getControladorPublishPort();
									String[] categories = port.listarCategoriasPF(programa.getNombre());
									if(categories.length != 0){
										for(String cat: categories){
											%><li class="list-group-item"><%=cat%></li><%
											System.out.println("Este programa tiene categorias" + cat);
										}
									}
									else{
										out.println("<div class='col-sm-12'><div class='alert alert-danger' role='alert'>No hay categorias asociadas al Programa.</div>");
									}%>
									
  									
									</ul>
				</div>
			</div>
		</div>
		</div>
						
		
		
		<%@include file="footer.jsp"%>
		<script>
			function _(id) {
				return document.getElementById(id);
			}
			function submitForm() {
				var formData = new FormData(_("form"));
				var ajax = new XMLHttpRequest();
				ajax.open("POST", "AltaCurso", true);

		    	$('#exampleModal').modal('hide')
				ajax.onreadystatechange = function() {
					if (this.readyState == 4 && this.status == 200) {
						if (this.responseText == "Exito!") {
							document.getElementById("mensajes").innerHTML = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Mensaje: </strong> "
									+ "El Curso fue dado de alta."
									+ "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
						} else {
							document.getElementById("mensajes").innerHTML = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Mensaje: </strong> "
									+ "Ya existe un Curso con ese nombre."
									+ "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
						}
						document.getElementById("form").reset();
					}
					;
				}
				ajax.send(formData);
			}
			
		</script>
		
		<script>
		function _(id) {
			return document.getElementById(id);
		}
		function submitFormProg() {
			var formData = new FormData(_("formProg"));
			var ajax = new XMLHttpRequest();
			ajax.open("POST", "AltaProgramaDeFormacion", true);

			ajax.onreadystatechange = function() {
			    if (this.readyState == 4 && this.status == 200) {
			    	var dev = this.responseText.replace("Served at: /edExtWeb", "");
			    	var res = dev.split("?");
			    	$('#agregarModalPrograma').modal('hide')
			    	if(res[0] == "ok"){
			    		document.getElementById("mensajes").innerHTML = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Mensaje: </strong> " + res[1] + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
			    	} else {
			    		document.getElementById("mensajes").innerHTML = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Mensaje: </strong> " + res[1] + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
			    	}
			    		    				   
			    };
			}
			ajax.send(formData);
		}
		</script>



</body>
</html>