<%@page contentType="text/html;charset=UTF-8"%><HTML>
<HEAD>
<TITLE>Methods</TITLE>
</HEAD>
<BODY>
<H1>Methods</H1>
<UL>
<LI><A HREF="Input.jsp?method=2" TARGET="inputs"> getEndpoint()</A></LI>
<LI><A HREF="Input.jsp?method=5" TARGET="inputs"> setEndpoint(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=10" TARGET="inputs"> getControladorPublish()</A></LI>
<LI><A HREF="Input.jsp?method=15" TARGET="inputs"> seleccionarCursoInstancia(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=20" TARGET="inputs"> seleccionarDtDocente(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=27" TARGET="inputs"> altaUsuarioEstudiante(java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.util.Calendar,boolean,java.lang.String,java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=46" TARGET="inputs"> mostrarDtEdicionesArray(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=51" TARGET="inputs"> rutaAnombreDeImagen(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=56" TARGET="inputs"> listarInstitutosArray()</A></LI>
<LI><A HREF="Input.jsp?method=59" TARGET="inputs"> listarNombreCursos(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=64" TARGET="inputs"> listarUsuarios()</A></LI>
<LI><A HREF="Input.jsp?method=67" TARGET="inputs"> seleccionarUsuario(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=90" TARGET="inputs"> listarProgramas()</A></LI>
<LI><A HREF="Input.jsp?method=93" TARGET="inputs"> modificarDatos(java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.util.Calendar)</A></LI>
<LI><A HREF="Input.jsp?method=106" TARGET="inputs"> cursosDePrograma()</A></LI>
<LI><A HREF="Input.jsp?method=109" TARGET="inputs"> seleccionarCurso(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=130" TARGET="inputs"> loginNickname(java.lang.String,java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=137" TARGET="inputs"> loginEmail(java.lang.String,java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=144" TARGET="inputs"> altaUsuarioDocente(java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.util.Calendar,boolean,java.lang.String,java.lang.String,java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=165" TARGET="inputs"> seleccionarDtUsuario(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=184" TARGET="inputs"> seleccionarInscripcionED(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=189" TARGET="inputs"> listarEdicionesdeCurso(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=194" TARGET="inputs"> nuevoCurso(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=199" TARGET="inputs"> setearCurso(java.lang.String,java.lang.String,java.lang.String,int,int,java.lang.String,java.util.Calendar,java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=218" TARGET="inputs"> seleccionarInstituto(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=223" TARGET="inputs"> listarCategoriasList()</A></LI>
<LI><A HREF="Input.jsp?method=226" TARGET="inputs"> obtenerInscripciones(java.lang.String,java.lang.String,java.lang.String,boolean)</A></LI>
<LI><A HREF="Input.jsp?method=237" TARGET="inputs"> inscrpcionesAceptadas(java.lang.String,java.lang.String,java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=246" TARGET="inputs"> mostrarDatosdeCurso(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=267" TARGET="inputs"> listarCategorias()</A></LI>
<LI><A HREF="Input.jsp?method=270" TARGET="inputs"> programasdeCurso(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=275" TARGET="inputs"> pruebaDate(java.util.Calendar)</A></LI>
<LI><A HREF="Input.jsp?method=280" TARGET="inputs"> institutoDeCurso(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=285" TARGET="inputs"> mostrarEdicion(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=302" TARGET="inputs"> cursosdeCategoria(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=307" TARGET="inputs"> altaInscripcionEdc(java.lang.String,java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=314" TARGET="inputs"> emailToNick(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=319" TARGET="inputs"> listarDocentes()</A></LI>
<LI><A HREF="Input.jsp?method=322" TARGET="inputs"> altaPrograma(java.lang.String,java.lang.String,java.util.Calendar,java.util.Calendar,java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=335" TARGET="inputs"> checkSeguidor(java.lang.String,java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=342" TARGET="inputs"> dejarSeguirUsuario(java.lang.String,java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=349" TARGET="inputs"> listarCategoriasPF(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=354" TARGET="inputs"> verPrograma(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=367" TARGET="inputs"> seguirUsuario(java.lang.String,java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=374" TARGET="inputs"> usuariosSeguidos(java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=379" TARGET="inputs"> traerUltimaEdicion(java.lang.String,java.lang.String)</A></LI>
<LI><A HREF="Input.jsp?method=398" TARGET="inputs"> confirmarAltaCurso()</A></LI>
</UL>
</BODY>
</HTML>