		<%@page import="publicadores.DtUsuario"%>
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
		  <a class="navbar-brand" href="index.jsp">
		    <img src="img/icono.png" width="30" height="30" alt="Logo">
		  </a>		  
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
	
		  <div class="collapse navbar-collapse" id="navbarSupportedContent2">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item">
	   		    <form class="form-inline ml-2">
			      <input class="form-control mr-sm-2" type="search" placeholder="" id="cursoABuscar" aria-label="Buscar">
			      <button class="btn btn-outline-success my-2 my-sm-0" disabled>Buscar</button>
			    </form>
		    	</li>
		    </ul>
			<ul class="navbar-nav ml-auto">
		      <li class="nav-item active">		  
		    		<a class="nav-link d-inline" href="#"><%=sesion.getAttribute("nombre")%> <%=sesion.getAttribute("apellido")%>
		    			<img src="imagenes/<%=sesion.getAttribute("nombreImagen")%>" class='img-usuario rounded-circle' alt='Imagen Perfil'> 															
		    		</a>
		    		<a href='LogOut' class=" nav-link d-inline">
			    		<img src="img/logout.svg"  class="logout"alt='LogOut'>
		    		</a>		    		
		    	</li>
		    </ul>		    
		  </div>
		</nav>
